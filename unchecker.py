#!/usr/bin/python3

import gkeepapi
import json
import os

keep = gkeepapi.Keep()

try:
    print("trying to load .keepstate")
    fh = open('.keepstate', 'r')
    state = json.load(fh)
    keep.restore(state)
except OSError:
    pass

user = os.getenv("UNCHECKER_EMAIL")
password = os.getenv("UNCHECKER_PASSWORD")
search = os.getenv("UNCHECKER_SEARCH")
print("logging in")
success = keep.login(user, password)
if not success:
    print("failed to login")
    exit()

gnotes = keep.find(query=search)
try:
    note = next(gnotes)
except StopIteration:
    print("not enough items found")
    exit()

try:
    next(gnotes)
    print("too many items found")
    exit()
except StopIteration:
    pass

for i in note.items:
    i.checked = False

print("syncing to google")
keep.sync()

try:
    print("trying to save .keepstate")
    state = keep.dump()
    fh = open('.keepstate', 'w')
    json.dump(state, fh)
except OSError as e:
    print("failed to create state file, ", e)
